const keyMain = '356b9f57ae683f0a3b8945d3bd624364';
const endpoint = 'api.openweathermap.org/data/2.5/weather?q=';
const formElement = document.querySelector('#form');
const searchInput = document.querySelector('#search-bar');
const resultsContainer = document.querySelector('#results');

let storeWeather = [];

formElement.addEventListener('submit', function(e){
    e.preventDefault();
    resultsContainer.textContent = '';
    let value = searchInput.value;
    storeData(getWeatherData(value));
    clearResults();
});

formElement.addEventListener('reset', function(e){
    clearResults();
});

function clearResults() {
    resultsContainer.textContent = "";
    searchInput.textContent = '';
    searchInput.value = '';
    storeWeather = [];
}


async function getWeatherData(location) {
    try {
        let response = await fetch(`https://api.openweathermap.org/data/2.5/weather?q=${location}&units=metric&APPID=356b9f57ae683f0a3b8945d3bd624364`);
        if (!response.ok) {
            throw new Error("Network response was not OK");
        }
        let results = await response.json();
        // console.log("Success:", results);
        return results;
    } catch(err) {
        console.error("Error:", err);
        let errorWarning = document.createElement('h2');
        errorWarning.textContent = 'The results are invalid';
        resultsContainer.insertAdjacentElement('afterbegin', errorWarning);
    }
}

async function storeData(data) {
    let info = await data;
    storeWeather.push({"city": info.name});
    storeWeather.push(info.main);
    storeWeather.push(info.weather[0]);
    storeWeather.push(info.wind);
    console.log(storeWeather);

    // function to populate the data
    populateData(storeWeather);
}

function populateData(arr) {
    console.log('Name:', arr[0].city);
    let markup;

    markup = `
    <h2>${arr[0].city}</h2>
    <div class="main-info card">
        <div class="main--icon">
            <img src="img/icon-${arr[2].main}.png" />
        </div>
        <p><span>Prediction</span>${arr[2].main}</p>
        <p><span>Temperature</span>${arr[1].temp}<span class="unit">ºC</span></p>
    </div>
    <div class="secondary-info card">
        <p><span>Feels like</span>${arr[1].feels_like}<span class="unit">ºC</span></p>
        <p><span>Min</span>${arr[1].temp_min}<span class="unit">ºC</span></p>
        <p><span>Max</span>${arr[1].temp_max}<span class="unit">ºC</span></p>
    </div>
    <div class="additional-info card">
        <p><span>Pressure</span>${arr[1].pressure}<span class="unit">Pa</span></p>
        <p><span>Humidity</span>${arr[1].humidity}<span class="unit">%</span></p>
        <p><span>Wind Speed</span>${arr[3].speed}<span class="unit">km/h</span></p>
        <p><span>Wind Degree</span>${arr[3].deg}<span class="unit">deg</span></p>
    </div>
    `;
    let df = document.createElement('div');
    df.setAttribute('class', 'weather-rendered');
    df.innerHTML = markup;
    resultsContainer.insertAdjacentElement('beforeend', df);
}
